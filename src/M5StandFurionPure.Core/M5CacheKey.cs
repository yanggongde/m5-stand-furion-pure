﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core
{
    public static class M5CacheKey
    {
        /// <summary>
        /// aop缓存key
        /// </summary>
        public static string Aop = "AopCache:{0}";

        /// <summary>
        /// 限制单点登录缓存key,如果多端需要限制，用同一个
        /// </summary>
        public static string SingleLoginById = "singleLoginCache:{0}";
    }
}
