﻿using FreeSql.DataAnnotations;
using M5Extend.Extensions;
using M5Extend.Tool;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.DbEntity
{
    /// <summary>
    /// 操作日志
    /// </summary>
    [Table(Name = "SystemOperationLog")]
    public class OperationLog
    {
        [Column(IsPrimary = true, IsIdentity = false)]
        public long Id { get; set; }

        /// <summary>
        /// 登录用户
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// 身份
        /// </summary>
        [MaxLength(1000)]
        public string Token { get; set; }


        /// <summary>
        /// 服务名称
        /// </summary>
        [MaxLength(500)]
        public string ServiceName { get; set; }

        /// <summary>
        /// 方法名称
        /// </summary>
        [MaxLength(250)]
        public string MethodName { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [MaxLength(2000)]
        public string Parameters { get; set; }

        /// <summary>
        /// 执行时间
        /// </summary>
        public DateTime ExecutionTime { get; set; }

        /// <summary>
        /// 持续时间 毫秒
        /// </summary>
        public int ExecutionDuration { get; set; }

        /// <summary>
        /// ip
        /// </summary>
        [MaxLength(500)]
        public string ClientIp { get; set; }

        /// <summary>
        /// ip地址
        /// </summary>
        [MaxLength(500)]
        public string ClientIpAddress { get; set; }

        /// <summary>
        /// 账户名称
        /// </summary>
        [MaxLength(250)]
        public string ClientName { get; set; }

        /// <summary>
        /// 请求路由
        /// </summary>
        [MaxLength(2000)]
        public string ClientUrl { get; set; }

        /// <summary>
        /// 浏览器信息
        /// </summary>
        [MaxLength(1000)]
        public string BrowserInfo { get; set; }


        /// <summary>
        /// 异常
        /// </summary>
        [MaxLength(4000)]
        public string Exception { get; set; }


        /// <summary>
        /// 调用方法
        /// </summary>
        [MaxLength(500)]
        public string MethodSummary { get; set; }

    }
}



//SET ANSI_NULLS ON
//GO
//SET QUOTED_IDENTIFIER ON
//GO
//CREATE TABLE [dbo].[SystemOperationLog](

//[Id][bigint] NOT NULL,

//[UserId] [bigint] NULL,
//	[Token] [varchar](1000) NULL,
//	[ServiceName] [varchar](500) NOT NULL,

//    [MethodName] [varchar](250) NOT NULL,

//    [MethodSummary] [nvarchar](500) NULL,
//	[Parameters] [nvarchar](2000) NULL,
//	[ExecutionTime] [datetime] NOT NULL,

//    [ExecutionDuration] [int] NOT NULL,

//    [ClientIp] [varchar](50) NULL,
//	[ClientIpAddress] [nvarchar](500) NULL,
//	[ClientName] [nvarchar](250) NULL,
//	[ClientUrl] [nvarchar](2000) NOT NULL,

//    [BrowserInfo] [nvarchar](1000) NOT NULL,

//    [Exception] [nvarchar](4000) NULL,
// CONSTRAINT[PK_SystemOperationLog] PRIMARY KEY CLUSTERED 
//(

//    [Id] ASC
//)WITH(PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]
//) ON[PRIMARY]
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'标识列' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'Id'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'登录用户ID' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'UserId'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'身份token' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'Token'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'服务名称' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ServiceName'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'方法名称' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'MethodName'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'方法注释' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'MethodSummary'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'请求参数' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'Parameters'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'执行时间' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ExecutionTime'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'执行持续时间 单位毫秒' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ExecutionDuration'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'ip' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ClientIp'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'ip地址' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ClientIpAddress'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'账户名称' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ClientName'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'请求路由' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'ClientUrl'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'浏览器信息' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'BrowserInfo'
//GO
//EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value = N'异常' , @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SystemOperationLog', @level2type = N'COLUMN', @level2name = N'Exception'
//GO