﻿using FreeSql.DataAnnotations;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.DbEntity
{
    public class User
    {
        [Column(IsPrimary = true, IsIdentity = false)]
        public long Id { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }

        public bool IsDeleted { get; set; }

    }
}
