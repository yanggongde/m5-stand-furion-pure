﻿using FreeSql.DataAnnotations;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.DbEntity
{
    public class IdT
    {
        [Column(IsPrimary = true, IsIdentity = false)]
        public long Id { get; set; }
    }
}
