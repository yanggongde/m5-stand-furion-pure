﻿using FreeSql.DataAnnotations;
using M5Extend.Extensions;
using M5Extend.Tool;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.DbEntity
{
    /// <summary>
    /// 节点
    /// </summary>
    [Table(Name = "tree_node")]
    public class Tree_node
    {
        [Column(IsPrimary = true, IsIdentity = false)]
        public long id { get; set; }

        /// <summary>
        /// 父级ID
        /// </summary>
        public long pid { get; set; }

        public string name { get; set; }
    }
}
