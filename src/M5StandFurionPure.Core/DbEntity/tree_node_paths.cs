﻿using FreeSql.DataAnnotations;
using M5Extend.Extensions;
using M5Extend.Tool;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.DbEntity
{
    /// <summary>
    /// 节点路径
    /// </summary>
    [Table(Name = "tree_node_paths")]
    public class Tree_node_paths
    {
        /// <summary>
        /// 祖先ID
        /// </summary>
        [Column(IsPrimary = true, IsIdentity = false)]
        public long ancestor { get; set; }

        /// <summary>
        /// 孙子id
        /// </summary>
        [Column(IsPrimary = true, IsIdentity = false)]
        public long descendant { get; set; }

        /// <summary>
        /// 隔了几代 0表示当前自己 1表示直系
        /// </summary>
        public int distance { get; set; }
    }
}
