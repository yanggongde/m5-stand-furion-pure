﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.DbEntity.MongoDB
{
    public class People : IMongoDBEntity<long>
    {
        [BsonId]
        public long Id { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }

    public class ChatMessage : IMongoDBEntity<long>
    {
        [BsonId]
        public long Id { get; set; }

        /// <summary>
        /// 会话ID
        /// </summary>
        public long ChatSessionId { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Contents { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime SendTime { get; set; }

        /// <summary>
        /// 发送者ID
        /// </summary>
        public long SendId { get; set; }

        /// <summary>
        /// 接收者ID
        /// </summary>
        public long ReceiveId { get; set; }

        /// <summary>
        /// 接收者是否已读
        /// </summary>
        public bool IsRead { get; set; }
    }
}
