﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Aop.Filter
{
    public static class AopFilterExtensions
    {
        public static void AddAopFilter(this MvcOptions options)
        {
            options.Filters.Add<DuplicationFilter>();
            options.Filters.Add<OperationLogFilter>();
            options.Filters.Add<UnitOfWorkFilter>();
        }
    }
}
