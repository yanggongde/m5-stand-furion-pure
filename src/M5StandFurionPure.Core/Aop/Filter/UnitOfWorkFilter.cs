﻿using FreeSql;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Data.SqlClient;
using SqlSugar;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Aop.Filter;

/// <summary>
/// SqlSugar 工作单元拦截器
/// </summary>
public class UnitOfWorkFilter : IAsyncActionFilter, IOrderedFilter
{
    /// <summary>
    /// 排序属性
    /// </summary>
    public int Order => 9999;

    /// <summary>
    /// 工作单元
    /// </summary>
    private readonly UnitOfWorkManager _unitOfWorkManager;

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="sqlSugarClient"></param>

    public UnitOfWorkFilter(UnitOfWorkManager unitOfWorkManager)
    {
        _unitOfWorkManager = unitOfWorkManager;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="context"></param>
    /// <param name="next"></param>
    /// <returns></returns>
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        // 获取动作方法描述器
        var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
        var method = actionDescriptor.MethodInfo;

        // 判断是否贴有工作单元特性
        if (!method.IsDefined(typeof(UnitOfWorkAttribute), true))
        {
            // 调用方法
            _ = await next();
        }
        else
        {
            var attribute = (method.GetCustomAttributes(typeof(UnitOfWorkAttribute), true).FirstOrDefault() as UnitOfWorkAttribute);
            if (attribute != null)
            {
                using (IUnitOfWork unitOfWork = _unitOfWorkManager.Begin(attribute.Propagation, attribute.IsolationLevel))
                {
                    var resultContext = await next();

                    if (resultContext.Exception == null)
                    {
                        try
                        {
                            unitOfWork.Commit();
                        }
                        catch
                        {
                            unitOfWork.Rollback();
                        }
                    }
                    else
                    {
                        unitOfWork.Rollback();
                    }
                }

            }
        }
    }
}
