﻿using CSRedis;
using M5Extend.Extensions;
using M5Standard.Core;
using M5Standard.Core.Aop;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Aop.Filter
{
    /// <summary>
    /// 防重提交拦截
    /// </summary>
    public class DuplicationFilter : IAsyncActionFilter, IOrderedFilter
    {
        private readonly CSRedisClient _redis;
        public DuplicationFilter(CSRedisClient redis)
        {
            _redis = redis;
        }
        public int Order => 9990;

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var method = actionDescriptor.MethodInfo;
            // 判断是否手动提交
            var isDefind = method.IsDefined(typeof(DuplicationAttribute), true);
            if (!isDefind || method == null)
            {
                await next();
            }
            else
            {
                var definds = method.GetCustomAttribute<DuplicationAttribute>();
                var lockKey = method.BuildAopFormatKey(context.ActionArguments, definds.Key, definds.KeyFormatNames);
                var locked = true;

                try
                {
                    if (!await _redis.M5_SetNXAsync(lockKey, lockKey, definds.ExpireSecond))
                    {
                        locked = false;
                        throw new LogicException(string.IsNullOrEmpty(definds.ErrorMessage) ? M5Consts.Cache.DefaultAuplicationMessage : definds.ErrorMessage);
                    }
                    else
                    {
                        await next();
                    }
                }
                catch (Exception exception)
                {
                    var message = string.IsNullOrEmpty(exception.Message) ?
                        exception.InnerException?.Message :
                        exception.Message;
                    throw new LogicException(message, exception);
                }
                finally
                {
                    if (locked)
                    {
                        //确保在执行完毕后删除锁
                        await _redis.DelAsync(lockKey);
                    }
                }
            }
        }
    }
}
