﻿using Furion;
using M5Extend.Extensions;
using M5Extend.IP2Region;
using M5Extend.SqlSugar;
using M5Extend.Tool;
using M5StandFurionPure.Core.DbEntity;
using M5StandFurionPure.Core.Output;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Aop.Filter
{
    /// <summary>
    /// 操作日志拦截
    /// </summary>
    public class OperationLogFilter : IAsyncActionFilter, IOrderedFilter
    {
        private readonly IFreeSql _sql;
        private readonly IpSearcher _ipSearcher;

        public OperationLogFilter(IFreeSql sql,
            IpSearcher ipSearcher)
        {
            _sql = sql;
            _ipSearcher = ipSearcher;
        }
        public int Order => 9996;

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var method = actionDescriptor.MethodInfo;
            // 判断是否手动提交
            var isDefind = method.IsDefined(typeof(OperationLogAttribute), true);
            if (!isDefind || method == null)
            {
                await next();
            }
            else
            {
                var definds = method.GetCustomAttribute<OperationLogAttribute>();
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                var resultContext = await next();
                if (resultContext.Exception == null)
                {
                    stopwatch.Stop();
                    await SaveLogAsync(method, context.ActionArguments, stopwatch, definds);
                }
                else
                {
                    stopwatch.Stop();
                    var exceptionMessage = $"{resultContext.Exception.Message}\n{resultContext.Exception.StackTrace}";
                    await SaveExceptionLogAsync(method, context.ActionArguments, stopwatch, exceptionMessage, definds);
                }                
            }
        }

        private OperationLog BuildOperationLog(MethodInfo method, IDictionary<string, object?> args, Stopwatch stopwatch, OperationLogAttribute defind)
        {
            var user = JwtLoginUser.GetWebLoginUser();
            var ip = App.HttpContext.M5_GetClientIP();
            return new OperationLog
            {
                Id = M5_Snowflake.CreateId(),
                BrowserInfo = App.HttpContext.M5_GetClientBrowser(),
                ClientIp = ip,
                ClientIpAddress = _ipSearcher.BinarySearch(ip)?.Region?.M5_ResolvIpAddress(),
                ClientUrl = FilterLongStr(App.HttpContext.Request.Path, 2000),
                ClientName = FilterLongStr(user?.UserName, 250),
                Exception = "",
                ExecutionDuration = stopwatch.Elapsed.TotalMilliseconds.M5_ConvertInt(),
                ExecutionTime = DateTime.Now,
                MethodName = FilterLongStr(method.Name, 250),
                ServiceName = FilterLongStr(method.DeclaringType.FullName, 250),
                MethodSummary = FilterLongStr(defind.ActionSummary, 500),
                Parameters = FilterParameters(args, defind),
                Token = user?.Token,
                UserId = user?.UserId
            };
        }

        private OperationLog BuildOperationExceptionLog(MethodInfo method, IDictionary<string, object?> args, Stopwatch stopwatch, string exmessage, OperationLogAttribute defind)
        {
            var user = JwtLoginUser.GetWebLoginUser();
            var ip = App.HttpContext.M5_GetClientIP();
            return new OperationLog
            {
                Id = M5_Snowflake.CreateId(),
                BrowserInfo = App.HttpContext.M5_GetClientBrowser(),
                ClientIp = ip,
                ClientIpAddress = _ipSearcher.BinarySearch(ip)?.Region?.M5_ResolvIpAddress(),
                ClientUrl = FilterLongStr(App.HttpContext.Request.Path, 2000),
                ClientName = FilterLongStr(user?.UserName, 250),
                Exception = FilterLongStr(exmessage, 4000),
                ExecutionDuration = stopwatch.Elapsed.TotalMilliseconds.M5_ConvertInt(),
                ExecutionTime = DateTime.Now,
                MethodName = FilterLongStr(method.Name, 250),
                ServiceName = FilterLongStr(method.DeclaringType.FullName, 250),
                MethodSummary = FilterLongStr(defind.ActionSummary, 500),
                Parameters = FilterParameters(args, defind),
                Token = user?.Token,
                UserId = user?.UserId
            };
        }


        /// <summary>
        /// 保存日志
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="summary"></param>
        private async Task SaveLogAsync(MethodInfo method, IDictionary<string, object?> args, Stopwatch stopwatch, OperationLogAttribute defind)
        {
            var log = BuildOperationLog(method, args, stopwatch, defind);
            await _sql.GetRepository<OperationLog>().InsertAsync(log);
        }

        /// <summary>
        /// 保存异常日志
        /// </summary>
        /// <param name="invocation"></param>
        /// <param name="summary"></param>
        private async Task SaveExceptionLogAsync(MethodInfo method, IDictionary<string, object?> args, Stopwatch stopwatch, string exmessage, OperationLogAttribute defind)
        {
            var log = BuildOperationExceptionLog(method, args, stopwatch, exmessage, defind);
            await _sql.GetRepository<OperationLog>().InsertAsync(log);
        }


        /// <summary>
        /// 过滤日志中的密码明文
        /// </summary>
        /// <param name="auditInfo"></param>
        /// <returns></returns>
        private string FilterParameters(IDictionary<string, object?> args, OperationLogAttribute defind)
        {
            if (defind.IgnoreParameter)
            {
                return string.Empty;
            }

            var argsStr = args.M5_ObjectToJson();
            if (argsStr.Length > 5000)
            {
                argsStr = argsStr.Substring(0, 5000);
            }
            return argsStr;
        }

        /// <summary>
        /// 过滤字符串长度
        /// </summary>
        /// <param name="str"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private string FilterLongStr(string str, int size)
        {
            if (string.IsNullOrEmpty(str))
            {
                return "";
            }

            if (str.Length > size)
            {
                str = str.Substring(0, size);
            }

            return str;
        }
    }
}
