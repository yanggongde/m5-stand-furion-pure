﻿using FreeSql;
using System;
using System.Data;

namespace M5StandFurionPure.Core.Aop;

/// <summary>
/// SqlSugar 工作单元配置特性[自动数据事务]
/// 注意生效范围：Controller，QuartzJob，Services  
/// 手动开启事务工作单元 UnitOfWorkManager.Begin() FreeSql 的工作单元自动管理跨方法跨但事务
/// 事务操作建议还是遵循：晚开早关
/// </summary>
[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true)]
public class UnitOfWorkAttribute : Attribute
{



    /// <summary>
    /// 构造函数
    /// </summary>
    public UnitOfWorkAttribute()
    {
        IsolationLevel = IsolationLevel.ReadCommitted;
        Propagation = Propagation.Required;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="isolationLevel">事务隔离级别</param>
    public UnitOfWorkAttribute(IsolationLevel isolationLevel)
    {
        IsolationLevel = isolationLevel;
        Propagation = Propagation.Required;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="isolationLevel">事务隔离级别</param>
    public UnitOfWorkAttribute(IsolationLevel isolationLevel, Propagation propagation)
    {
        IsolationLevel = isolationLevel;
        Propagation = propagation;
    }



    /// <summary>
    /// 事务隔离级别
    /// </summary>
    /// <remarks>
    /// <para>默认：<see cref="IsolationLevel.ReadCommitted"/>，参见：<see cref="IsolationLevel"/></para>
    /// <para>说明：当事务A更新某条数据的时候，不容许其他事务来更新该数据，但可以进行读取操作</para>
    /// </remarks>
    public IsolationLevel IsolationLevel { get; set; } = IsolationLevel.ReadCommitted;

    /// <summary>
    /// 
    /// </summary>
    public Propagation Propagation { get; set; } = Propagation.Required;

}
