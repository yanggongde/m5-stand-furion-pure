﻿using M5Standard.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Aop
{
    /// <summary>
    /// 标注方法防重提交
    /// 生效范围：Services Controller,建议Services和Controller标记一处即可实现接口幂等请求
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class DuplicationAttribute : Attribute
    {
        /// <summary>
        /// 制定防重提交的key 不填则采用自动构建（方法fullName+md5(参数+token)）
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 制定防重keyfromat 参数Name
        /// </summary>
        public string[] KeyFormatNames { get; set; }

        /// <summary>
        /// 锁时长 单位/秒 不传则默认30秒,自动续期
        /// </summary>
        public int ExpireSecond { get; set; }

        /// <summary>
        /// 错误消息,不传采用默认
        /// </summary>
        public string ErrorMessage { get; set; }

        public DuplicationAttribute()
        {
            ExpireSecond = M5Consts.Cache.DefaultLockDisponseSecond;
        }

        public DuplicationAttribute(string key)
        {
            Key = key;
            ExpireSecond = M5Consts.Cache.DefaultLockDisponseSecond;
        }

        public DuplicationAttribute(int expireSecond)
        {
            ExpireSecond = expireSecond;
        }

        public DuplicationAttribute(string key, params string[] keyFormatNames)
        {
            Key = key;
            KeyFormatNames = keyFormatNames;
            ExpireSecond = M5Consts.Cache.DefaultLockDisponseSecond;
        }

        public DuplicationAttribute(string key, int expireSecond)
        {
            Key = key;
            KeyFormatNames = null;
            ExpireSecond = expireSecond;
        }

        public DuplicationAttribute(string key, int expireSecond, params string[] keyFormatNames)
        {
            Key = key;
            KeyFormatNames = keyFormatNames;
            ExpireSecond = expireSecond;
        }
    }
}
