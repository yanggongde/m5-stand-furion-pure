﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M5StandFurionPure.Core.Aop
{
    /// <summary>
    /// 标识记录业务操作日志
    /// 生效范围：Services Controller,建议Services和Controller标记一处即可，如果需要同时记录controller和service的日志可以同时标记
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class OperationLogAttribute : Attribute
    {
        /// <summary>
        /// 请求方法的模块注释
        /// </summary>
        public string ActionSummary { get; set; }

        /// <summary>
        /// 忽略请求参数记录（可用于密码等关键参数记录忽略）
        /// </summary>
        public bool IgnoreParameter { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="minutes"></param>
        public OperationLogAttribute(string actionSummary)
        {
            ActionSummary = actionSummary;
            IgnoreParameter = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minutes"></param>
        public OperationLogAttribute(string actionSummary, bool ignoreParameter)
        {
            ActionSummary = actionSummary;
            IgnoreParameter = ignoreParameter;
        }
    }
}
