﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Aop
{

    /// <summary>
    /// 标注方法根据返回值缓存数据
    /// 生效范围：Services
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class CachedAttribute : Attribute
    {
        /// <summary>
        /// 缓存的key,不填则采用自动构建（方法fullName+md5(参数+token)）
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// key的format格式化制定参数name
        /// </summary>
        public string[] KeyFormatNames { get; set; }

        /// <summary>
        /// 缓存时长 单位/秒 不传则永久
        /// </summary>
        public int ExpireSecond { get; set; }

        public CachedAttribute()
        {
            ExpireSecond = -1;
        }

        public CachedAttribute(int expireSecond)
        {
            ExpireSecond = expireSecond;
        }

        public CachedAttribute(string key)
        {
            Key = key;
            ExpireSecond = -1;
        }

        public CachedAttribute(string key, params string[] keyFormatNames)
        {
            Key = key;
            KeyFormatNames = keyFormatNames;
            ExpireSecond = -1;
        }

        public CachedAttribute(string key, int expireSecond)
        {
            Key = key;
            KeyFormatNames = null;
            ExpireSecond = expireSecond;
        }

        public CachedAttribute(string key, int expireSecond, params string[] keyFormatNames)
        {
            Key = key;
            KeyFormatNames = keyFormatNames;
            ExpireSecond = expireSecond;
        }
    }
}
