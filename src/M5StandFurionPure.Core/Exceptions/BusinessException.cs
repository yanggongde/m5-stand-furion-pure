﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core
{
    /// <summary>
    /// 业务逻辑异常【默认日志记录会排除此类】
    /// </summary>
    [Serializable]
    public class LogicException : Exception
    {
        public LogicException()
        {
        }

        public LogicException(string message) : base(message)
        {
        }

        public LogicException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LogicException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    public class OperationLogException : LogicException
    {
        public OperationLogException(string message) : base(message)
        {
        }
        public OperationLogException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    public class UnauthorizedException : LogicException
    {
        public UnauthorizedException(string message) : base(message)
        {
        }
    }

    public class SingleLoginException : LogicException
    {
        public SingleLoginException(string message) : base(message)
        {
        }
    }

    public class NoPermissionException : LogicException
    {
        public NoPermissionException(string message) : base(message)
        {
        }
    }
}
