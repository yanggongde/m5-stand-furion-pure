﻿using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Logger
{
    public static class SerilogConfig
    {
        public static void WriteLogFileConfig(this LoggerConfiguration config, bool writeConsole, string applicationName, string environmentName = "")
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");//按时间创建文件夹
            string outputTemplate = "{NewLine}【{Level:u3}】{Timestamp:yyyy-MM-dd HH:mm:ss.fff}" +
            "{NewLine}#Msg#{Message:lj}" +
            "{NewLine}#Pro #{Properties:j}" +
            "{NewLine}#Exc#{Exception}" +
            new string('-', 50);//输出模板
            if (writeConsole)
            {
                config.WriteTo.Console(outputTemplate: outputTemplate);
            }

            //1.输出所有restrictedToMinimumLevel：LogEventLevel类型
            config
            //.MinimumLevel.Debug() // 所有Sink的最小记录级别
            //.MinimumLevel.Override("Microsoft", LogEventLevel.Fatal)
            //.Enrich.FromLogContext()
            //
                 .Enrich.WithProperty("ApplicationName", applicationName)
                 .Enrich.WithProperty("Environment", environmentName)
                .WriteTo.File($"log/{date}/Information.log",
                       outputTemplate: outputTemplate,
                        restrictedToMinimumLevel: LogEventLevel.Information,
                        rollingInterval: RollingInterval.Day,//日志按日保存，这样会在文件名称后自动加上日期后缀
                                                             //rollOnFileSizeLimit: true,          // 限制单个文件的最大长度
                                                             //retainedFileCountLimit: 10,         // 最大保存文件数,等于null时永远保留文件。
                                                             //fileSizeLimitBytes: 10 * 1024,      // 最大单个文件大小
                        encoding: Encoding.UTF8            // 文件字符编码
                    )

                //2.1仅输出 LogEventLevel.Debug 类型
                .WriteTo.Logger(lg => lg.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Debug)//筛选过滤
                    .WriteTo.File($"log/{date}/{LogEventLevel.Debug}.log",
                        outputTemplate: outputTemplate,
                        rollingInterval: RollingInterval.Day,//日志按日保存，这样会在文件名称后自动加上日期后缀
                        encoding: Encoding.UTF8            // 文件字符编码
                     )
                )


                //2.1仅输出 LogEventLevel.Warning 类型
                .WriteTo.Logger(lg => lg.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Warning)//筛选过滤
                    .WriteTo.File($"log/{date}/{LogEventLevel.Warning}.log",
                        outputTemplate: outputTemplate,
                        rollingInterval: RollingInterval.Day,//日志按日保存，这样会在文件名称后自动加上日期后缀
                        encoding: Encoding.UTF8            // 文件字符编码
                     )
               )

                //2.2仅输出 LogEventLevel.Error 类型
                .WriteTo.Logger(lg => lg.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Error)//筛选过滤
                    .WriteTo.File($"log/{date}/{LogEventLevel.Error}.log",
                        outputTemplate: outputTemplate,
                        rollingInterval: RollingInterval.Day,//日志按日保存，这样会在文件名称后自动加上日期后缀
                        encoding: Encoding.UTF8            // 文件字符编码
                     )
               );
        }
    }
}
