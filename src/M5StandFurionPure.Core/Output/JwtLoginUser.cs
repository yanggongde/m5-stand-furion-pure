﻿using CSRedis;
using Furion;
using Furion.DataEncryption;
using M5Extend.Extensions;
using M5Extend.IP2Region;
using M5Standard.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.JsonWebTokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Output
{
    [Serializable]
    public class SingleLogin
    {
        public string Token { get; set; }

        public string LoginTime { get; set; }

        public string LoginIp { get; set; }

        public string IpAddress { get; set; }

        public string DevicesName { get; set; }

        public string Message { get; set; }
    }

    public class BaseLoginUser
    {
        public long UserId { get; set; }

        public string Token { get; set; }

        public string UserName { get; set; }
    }
    public class WebLoginUser: BaseLoginUser
    {

    }

    public class TokenInfo
    {
        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 过期分钟
        /// </summary>
        public int ExpireMinutes { get; set; }
    }

    public static class JwtLoginUser
    {
        public const string TokenHeaderKey = "Authorization";

        /// <summary>
        /// 固定的 Claim 类型
        /// </summary>
        private static readonly string[] StationaryClaimTypes = new[] {
            JwtRegisteredClaimNames.Iat,
            JwtRegisteredClaimNames.Nbf,
            JwtRegisteredClaimNames.Exp,
            JwtRegisteredClaimNames.Iss,
            JwtRegisteredClaimNames.Aud
        };

        /// <summary>
        /// 获取当前登录用户ID
        /// </summary>
        /// <returns></returns>
        public static long GetUserId()
        {
            return (App.User?.FindFirstValue(nameof(BaseLoginUser.UserId))).M5_ParseLong();
        }

        /// <summary>
        /// 构建Web端登录token
        /// </summary>
        /// <returns></returns>
        public static TokenInfo BuildTokenForWebLoginUser(this WebLoginUser input)
        {
            var expireMinutes = GetAbsoluteExpireMinutesByWorkDay();
            if (!expireMinutes.HasValue)
            {
                expireMinutes = App.Configuration["JWTSettings:ExpiredTime"].M5_ParseLong();
            }
            var accessToken = JWTEncryption.Encrypt(new Dictionary<string, object>()
            {
                { nameof(WebLoginUser.UserId), input.UserId},
                { nameof(WebLoginUser.UserName), input.UserName},
            }, GetAbsoluteExpireMinutesByWorkDay());

            return new TokenInfo
            {
                Token = accessToken,
                ExpireMinutes = (int)expireMinutes.Value
            };
        }

        /// <summary>
        /// 颁发Token，携带单点登录认证
        /// </summary>
        /// <param name="logininfo"></param>
        /// <returns></returns>
        public static async Task<TokenInfo> BuildSinaleTokenForWebLoginUser(this WebLoginUser input)
        {
            var redis = App.RootServices.GetRequiredService<CSRedisClient>();
            var httpContextAccessor = App.RootServices.GetRequiredService<IHttpContextAccessor>();
            var ipSearcher = App.RootServices.GetRequiredService<IpSearcher>();

            var tokenInfo = input.BuildTokenForWebLoginUser();
            var isEnableSingleLogin = (App.Configuration["Aouth:EnableSingleLoginCheck"]).M5_ParseBoolean();
            if (isEnableSingleLogin)
            {
                var ip = httpContextAccessor.HttpContext!.M5_GetClientIP();
                var ipaddress = ipSearcher.BtreeSearch(ip)?.Region?.M5_ResolvIpAddress();

                //写入单点登录中心
                await redis.DelAsync(M5CacheKey.SingleLoginById.M5_Format(input.UserId));
                await redis.SetAsync(M5CacheKey.SingleLoginById.M5_Format(input.UserId), new SingleLogin
                {
                    LoginIp = ip,
                    LoginTime = DateTime.Now.ToString("MM月dd日 HH:mm:ss"),
                    Token = $"Bearer {tokenInfo?.Token}",
                    DevicesName = httpContextAccessor.HttpContext!.M5_GetBrowserName(),
                    IpAddress = ipaddress
                }, TimeSpan.FromMinutes(App.Configuration["JWTSettings:Lifetime"].M5_ParseInt() + 10));
            }

            return tokenInfo;
        }


        /// <summary>
        /// 获取web登录用户信息
        /// </summary>
        /// <returns></returns>
        public static WebLoginUser GetWebLoginUser()
        {
            var token = GetLogindToken();
            if (string.IsNullOrEmpty(token) || App.User == null)
            {
                return default;
            }

            return new WebLoginUser
            {
                Token = token,
                UserId = (App.User?.FindFirstValue(nameof(WebLoginUser.UserId))).M5_ParseLong(),
                UserName= App.User?.FindFirstValue(nameof(WebLoginUser.UserName))?.ToString()
            };
        }

        /// <summary>
        /// 获取web登录用户信息
        /// </summary>
        /// <returns></returns>
        public static WebLoginUser GetWebLoginUser(this string token)
        {
            if (!ValidateToken(token))
            {
                return default;
            }

            var payloads = GetTokenPayloads(token);

            return new WebLoginUser
            {
                Token = token,
                UserId = payloads.GetPayload(nameof(WebLoginUser.UserId)).M5_ParseLong(),
                UserName = payloads.GetPayload(nameof(WebLoginUser.UserName))?.ToString(),
            };
        }

        /// <summary>
        /// 获取已登录的token
        /// </summary>
        /// <returns></returns>
        public static string GetLogindToken()
        {
            var headers = App.HttpContext?.Request?.Headers?.ToList();
            if (headers == null || !headers.Any() || !headers.Any(k => k.Key == TokenHeaderKey))
            {
                return String.Empty;
            }

            return headers.FirstOrDefault(k => k.Key == TokenHeaderKey).Value;
        }

        private static object GetPayload(this Dictionary<string, object> playloads, string key)
        {
            if (playloads.Keys.Any(k => k == key))
            {
                return playloads.FirstOrDefault(k => k.Key == key).Value;
            }

            return default;
        }

        /// <summary>
        /// 获取token中的业务数据
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static Dictionary<string, object> GetTokenPayloads(string token)
        {
            var claims = JWTEncryption.ReadJwtToken(token)?.Claims?.ToList();
            if (claims == null || !claims.Any())
            {
                return new Dictionary<string, object>();
            }

            return claims.Where(u => !StationaryClaimTypes.Contains(u.Type))
                                     .ToDictionary(u => u.Type, u => (object)u.Value,
                                     new MultiClaimsDictionaryComparer()) ?? new Dictionary<string, object>();
        }

        /// <summary>
        /// 验证token的有效性
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool ValidateToken(string token)
        {
            var (_isValid, _, _) = JWTEncryption.Validate(token);
            return _isValid;
        }

        /// <summary>
        /// 构建token绝对过期时间
        /// </summary>
        /// <returns></returns>
        private static long? GetAbsoluteExpireMinutes(DateTime? absoluteTime)
        {
            //采用配置过期时间
            if (!absoluteTime.HasValue) return null;
            var now = DateTime.Now;
            //如果马上过期则采用配置过期时间
            if (absoluteTime.Value <= now) return null;
            //如果过期时间小于20分钟则采用配置过期时间
            if ((absoluteTime.Value - now).TotalMinutes <= 20) return null;
            return (long)(absoluteTime.Value - now).TotalMinutes;
        }

        /// <summary>
        /// 构建工作时间token绝对过期时间
        /// </summary>
        /// <returns></returns>
        private static long? GetAbsoluteExpireMinutesByWorkDay()
        {
            var now = DateTime.Now;
            if (now.Hour >= 20) return null;
            var absoluteExpireTime = new DateTime(now.Year, now.Month, now.Day, 20, 0, 0);
            return GetAbsoluteExpireMinutes(absoluteExpireTime);
        }

        /// <summary>
        /// 解决 Claims 身份重复键问题
        /// </summary>
        internal sealed class MultiClaimsDictionaryComparer : IEqualityComparer<string>
        {
            /// <summary>
            /// 设置字符串永不相等
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            public bool Equals(string x, string y)
            {
                return x != y;
            }

            /// <summary>
            /// 返回字符串 hashCode
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public int GetHashCode(string obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}
