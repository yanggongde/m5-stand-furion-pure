﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Furion.DataValidation;
using Furion.DependencyInjection;
using Furion.UnifyResult;
using Furion;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Reflection;
using M5Extend.Output;
using Furion.FriendlyException;

namespace M5StandFurionPure.Core
{
    /// <summary>
    /// RESTful 风格返回值
    /// </summary>
    [SuppressSniffer, UnifyModel(typeof(M5Result<>))]
    public class RESTfulResultProvider : IUnifyResultProvider
    {
        /// <summary>
        /// 异常返回值
        /// </summary>
        /// <param name="context"></param>
        /// <param name="metadata"></param>
        /// <returns></returns>
        public IActionResult OnException(ExceptionContext context, ExceptionMetadata metadata)
        {
            var exception = context.Exception;
            int statusCode = metadata.StatusCode;
            if (exception is UnauthorizedException) statusCode = 401;
            if (exception is SingleLoginException) statusCode = 402;
            if (exception is NoPermissionException) statusCode = 403;

            //var method = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
            return new JsonResult(GlobalOutput(statusCode, message: metadata.Errors.ToString()));
        }



        /// <summary>
        /// 成功返回值
        /// </summary>
        /// <param name="context"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public IActionResult OnSucceeded(ActionExecutedContext context, object data)
        {
            var method = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
            if (method.ReturnType == typeof(void) || method.ReturnType == typeof(Task))
                return new JsonResult(GlobalOutput(StatusCodes.Status200OK, "请求成功"));
            else
                return new JsonResult(GlobalOutput(StatusCodes.Status200OK, "请求成功", data));
        }

        /// <summary>
        /// 验证失败返回值
        /// </summary>
        /// <param name="context"></param>
        /// <param name="metadata"></param>
        /// <returns></returns>
        public IActionResult OnValidateFailed(ActionExecutingContext context, ValidationMetadata metadata)
        {
            //var method = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;

            return new JsonResult(GlobalOutput(StatusCodes.Status400BadRequest, message: metadata.Message));
        }

        /// <summary>
        /// 特定状态码返回值
        /// </summary>
        /// <param name="context"></param>
        /// <param name="statusCode"></param>
        /// <param name="unifyResultSettings"></param>
        /// <returns></returns>
        public async Task OnResponseStatusCodes(HttpContext context, int statusCode, UnifyResultSettingsOptions unifyResultSettings)
        {
            // 设置响应状态码
            UnifyContext.SetResponseStatusCodes(context, statusCode, unifyResultSettings);

            switch (statusCode)
            {
                // 处理 401 状态码
                case StatusCodes.Status401Unauthorized:
                    await context.Response.WriteAsJsonAsync(GlobalOutput(statusCode, message: "401 Unauthorized")
                        , App.GetOptions<JsonOptions>()?.JsonSerializerOptions);
                    break;
                case StatusCodes.Status402PaymentRequired:
                    await context.Response.WriteAsJsonAsync(GlobalOutput(statusCode, message: "402 PaymentRequired")
                        , App.GetOptions<JsonOptions>()?.JsonSerializerOptions);
                    break;
                // 处理 403 状态码
                case StatusCodes.Status403Forbidden:
                    await context.Response.WriteAsJsonAsync(GlobalOutput(statusCode, message: "403 Forbidden")
                        , App.GetOptions<JsonOptions>()?.JsonSerializerOptions);
                    break;
                default: break;
            }
        }


        /// <summary>
        /// 全局统一输出
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static M5Result<object> GlobalOutput(int code, string message, object data)
        {
            return new M5Result<object>
            {
                code = code,
                message = message,
                data = data
            };
        }

        /// <summary>
        /// 全局统一输出
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static M5Result GlobalOutput(int code, string message)
        {
            return new M5Result
            {
                code = code,
                message = message
            };
        }

    }
}