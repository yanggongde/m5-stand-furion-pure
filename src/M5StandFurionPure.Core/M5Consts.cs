﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5Standard.Core
{
    public static class M5Consts
    {
        public static class Cache
        {
            /// <summary>
            /// 默认锁释放时间  单位/秒
            /// </summary>
            public const int DefaultLockDisponseSecond = 180;

            /// <summary>
            /// 防重提交错误消息
            /// </summary>
            public const string DefaultAuplicationMessage = "系统繁忙，请稍后再试！";
        }
    }
}
