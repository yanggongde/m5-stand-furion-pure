﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Quartz
{
    /// <summary>
    /// 定义任务的执行方式
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class QuartzJobConfigAttribute : Attribute
    {
        /// <summary>
        /// 执行表达式执行
        /// </summary>
        /// <param name="cron"></param>
        public QuartzJobConfigAttribute(string cron)
        {
            Cron = cron.Trim();
            QuartzProjectType= QuartzProjectType.Background;
        }

        /// <summary>
        /// 按间隔时间执行
        /// </summary>
        /// <param name="cron"></param>
        public QuartzJobConfigAttribute(int intervalSecond)
        {
            IntervalSecond = intervalSecond;
            IntervalCounts = 0;
            QuartzProjectType = QuartzProjectType.Background;
        }

        /// <summary>
        /// 按间隔时间执行
        /// </summary>
        /// <param name="cron"></param>
        public QuartzJobConfigAttribute(int intervalSecond, int tntervalCounts)
        {
            IntervalSecond = intervalSecond;
            IntervalCounts = tntervalCounts;
            QuartzProjectType = QuartzProjectType.Background;
        }

        /// <summary>
        /// 所属项目
        /// </summary>
        public QuartzProjectType QuartzProjectType { get; set; } = QuartzProjectType.Background;

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 分组名称
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 任务分组
        /// </summary>
        public string TaskGroup { get; set; }

        /// <summary>
        /// cron 表达式
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// 循环执行次数
        /// </summary>
        public int IntervalCounts { get; set; }

        /// <summary>
        /// 执行时间间隔  单位/秒
        /// </summary>
        public int IntervalSecond { get; set; }

        /// <summary>
        /// 开始
        /// </summary>
        public DateTime? Start { get; set; }

        /// <summary>
        /// 结束
        /// </summary>
        public DateTime? End { get; set; }
    }

    public enum QuartzProjectType
    {
        /// <summary>
        /// 后台任务站点
        /// </summary>
        Background,

        /// <summary>
        /// 道闸中心
        /// </summary>
        RoadGate
    }
}
