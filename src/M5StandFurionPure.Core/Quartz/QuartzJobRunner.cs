﻿using FreeSql;
using M5Standard.Core.Aop;
using M5StandFurionPure.Core.Aop;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Quartz
{
    /// <summary>
    /// 解决范围注册的问题https://blog.csdn.net/qin_yu_2010/article/details/108657115
    /// </summary>
    public class QuartzJobRunner : IJob
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<QuartzJobRunner> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="sqlSugarClient"></param>
        /// <param name="capPublisher"></param>
        /// <param name="logger"></param>
        public QuartzJobRunner(IServiceProvider serviceProvider,
            ILogger<QuartzJobRunner> logger)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }


        public async Task Execute(IJobExecutionContext context)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var jobType = context.JobDetail.JobType;
                var job = scope.ServiceProvider.GetRequiredService(jobType) as IJob;

                //添加job任务的工作单元支持
                if (jobType.IsDefined(typeof(UnitOfWorkAttribute)))
                {
                    var _unitOfWorkManager = scope.ServiceProvider.GetRequiredService<UnitOfWorkManager>();
                    await ExecuteTransaction(_unitOfWorkManager, context, job, jobType);
                }
                else
                    await ExecuteNormal(context, job, jobType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="job"></param>
        /// <param name="jobType"></param>
        /// <returns></returns>
        private async Task ExecuteNormal(IJobExecutionContext context, IJob? job, Type jobType)
        {
            try
            {
                await job.Execute(context);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.InnerException == null ?
                                   ex.Message :
                                   ex.InnerException.Message;
                _logger.LogError(ex, $"{jobType.Name}任务执行失败：{exceptionMessage}");
                Console.WriteLine($"\n{jobType.Name}任务执行失败：{exceptionMessage}，详细错误请看日志。");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="job"></param>
        /// <param name="jobType"></param>
        /// <returns></returns>
        private async Task ExecuteTransaction(UnitOfWorkManager _unitOfWorkManager, IJobExecutionContext context, IJob? job, Type jobType)
        {
            var unitOfWorkOptions = jobType.GetCustomAttribute<UnitOfWorkAttribute>();
            if (unitOfWorkOptions != null)
            {

                using (IUnitOfWork unitOfWork = _unitOfWorkManager.Begin(unitOfWorkOptions.Propagation, unitOfWorkOptions.IsolationLevel))
                {
                    try
                    {
                        await job.Execute(context);
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        var exceptionMessage = ex.InnerException == null ?
                                       ex.Message :
                                       ex.InnerException.Message;
                        _logger.LogError(ex, $"{jobType.Name}任务执行失败：{exceptionMessage}");
                        Console.WriteLine($"\n{jobType.Name}任务执行失败：{exceptionMessage}，详细错误请看日志。");
                    }
                }

            }
            else
            {
                await ExecuteNormal(context, job, jobType);
            }
        }
    }
}
