﻿using Furion;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Quartz
{
    public static class QuartzExtensions
    {
        /// <summary>
        /// 自动添加所属项目的标识
        /// </summary>
        public static QuartzProjectType QuartzProjectType { get; private set; }

        public static void ConfigQuartz(this IServiceCollection services, QuartzProjectType quartzProjectType = QuartzProjectType.Background)
        {
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<IJobFactory, QuartzJobFactory>();
            services.AddSingleton<QuartzJobRunner>();
            services.AddSingleton<QuartzService>();
            QuartzProjectType = quartzProjectType;
        }

        public static void UseQuartz(this IApplicationBuilder app, IHostApplicationLifetime appLifetime)
        {
            var quartz = app.ApplicationServices.GetRequiredService<QuartzService>();
            appLifetime.ApplicationStarted.Register(() =>
            {
                quartz.Start().Wait();
            });

            appLifetime.ApplicationStopped.Register(() =>
            {
                //Quzrtz关闭方法
                quartz.Stop();
            });
        }

        public static async Task InitFiexdQuartzJob(this IScheduler scheduler, QuartzProjectType quartzProjectType)
        {
            var quarzTypes = App.EffectiveTypes
                .Where(k => typeof(IJob).IsAssignableFrom(k))
                .ToList() ?? new List<Type>();

            foreach (var k in quarzTypes)
            {
                if (k.IsDefined(typeof(QuartzJobConfigAttribute)))
                {
                    var defind = k.GetCustomAttribute<QuartzJobConfigAttribute>();
                    if (defind != null && defind.QuartzProjectType == quartzProjectType) await scheduler.ScheduleAsync(defind, k);
                }
            }
        }

        /// <summary>
        /// 自动添加作业
        /// </summary>
        /// <typeparam name="TJob"></typeparam>
        /// <param name="scheduler"></param>
        /// <param name="configureJob"></param>
        /// <param name="configureTrigger"></param>
        /// <returns></returns>
        public static async Task ScheduleAsync(this IScheduler scheduler, QuartzJobConfigAttribute defind, Type t)
        {
            var jobToBuild = JobBuilder.Create(t);
            var jobName = string.IsNullOrEmpty(defind.TaskName) ? t.Name : defind.TaskName;
            var groupName = string.IsNullOrEmpty(defind.GroupName) ? "default" : defind.GroupName;

            jobToBuild.WithIdentity(jobName, groupName).WithDescription($"{jobName}-job");
            var job = jobToBuild.Build();

            var triggerToBuild = TriggerBuilder.Create();
            if (!string.IsNullOrEmpty(defind.Cron) && CronExpression.IsValidExpression(defind.Cron))
            {
                triggerToBuild.WithCronSchedule(defind.Cron);
                if (defind.Start.HasValue) triggerToBuild.StartAt(defind.Start.Value);
                else triggerToBuild.StartNow();
                if (defind.End.HasValue) triggerToBuild.EndAt(defind.End.Value);
            }
            else
            {
                if (defind.IntervalSecond <= 0)
                {
                    throw new ArgumentNullException($"QuartzJob:{t.Name} 定义任务参数未初始化IntervalSecond参数");
                }

                if (defind.Start.HasValue) triggerToBuild.StartAt(defind.Start.Value);
                else triggerToBuild.StartNow();
                if (defind.End.HasValue) triggerToBuild.EndAt(defind.End.Value);
                if (defind.IntervalCounts > 0)
                {
                    triggerToBuild.WithSimpleSchedule(k =>
                    {
                        k.WithIntervalInSeconds(defind.IntervalSecond).WithRepeatCount(defind.IntervalCounts - 1);
                    });
                }
                else
                {
                    triggerToBuild.WithSimpleSchedule(k =>
                    {
                        k.WithIntervalInSeconds(defind.IntervalSecond).RepeatForever();
                    });
                }
            }

            var trigger = triggerToBuild.Build();
            await scheduler.ScheduleJob(job, trigger);
        }

        /// <summary>
        /// 手动添加作业
        /// </summary>
        /// <typeparam name="TJob"></typeparam>
        /// <param name="scheduler"></param>
        /// <param name="configureJob"></param>
        /// <param name="configureTrigger"></param>
        /// <returns></returns>
        public static async Task ScheduleAsync<TJob>(this IScheduler scheduler, Action<JobBuilder> configureJob, Action<TriggerBuilder> configureTrigger) where TJob : IJob
        {
            var jobToBuild = JobBuilder.Create<TJob>();
            configureJob.Invoke(jobToBuild);
            var job = jobToBuild.Build();

            var triggerToBuild = TriggerBuilder.Create();
            configureTrigger.Invoke(triggerToBuild);
            var trigger = triggerToBuild.Build();


            await scheduler.ScheduleJob(job, trigger);
        }
    }
}
