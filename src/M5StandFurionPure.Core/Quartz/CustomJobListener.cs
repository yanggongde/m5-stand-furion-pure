﻿using Quartz;
using Quartz.Listener;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Quartz
{

    public class CustomJobListener : IJobListener
    {
        public string Name => "CustomJobListener";

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            Console.WriteLine($"\n[{DateTime.Now}]  任务：【{context.JobDetail.Key.Name}】  执行被否决了!");
            return Task.CompletedTask;
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            Console.WriteLine($"\n[{DateTime.Now}]  任务：【{context.JobDetail.Key.Name}】  开始执行.");
            return Task.CompletedTask;
        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException? jobException, CancellationToken cancellationToken = default(CancellationToken))
        {
            Console.WriteLine($"\n[{DateTime.Now}]  任务：【{context.JobDetail.Key.Name}】  执行完毕.");
            return Task.CompletedTask;
        }
    }
}
