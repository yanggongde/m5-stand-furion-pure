﻿using Quartz;
using Quartz.Impl.Triggers;
using Quartz.Spi;

namespace M5StandFurionPure.Core.Quartz
{
    public class QuartzService
    {
        private ISchedulerFactory _schedulerFactory;
        private IScheduler _scheduler;
        private IJobFactory _IOCjobFactory;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schedulerFactory"></param>
        /// <param name="jobFactory"></param>
        public QuartzService(ISchedulerFactory schedulerFactory, IJobFactory jobFactory)
        {
            _schedulerFactory = schedulerFactory;
            _IOCjobFactory = jobFactory;
        }


        /// <summary>
        /// 启动
        /// </summary>
        /// <returns></returns>
        public async Task<string> Start()
        {
            _scheduler = await _schedulerFactory.GetScheduler();
            _scheduler.JobFactory = _IOCjobFactory;
            _scheduler.ListenerManager.AddJobListener(new CustomJobListener());
            await _scheduler.Start();
            await InitScheduleJob(_scheduler);
            return await Task.FromResult("将触发器和任务器绑定到调度器中完成");
        }


        /// <summary>
        /// 初始化job
        /// </summary>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public async Task InitScheduleJob(IScheduler scheduler)
        {
            await scheduler.InitFiexdQuartzJob(QuartzExtensions.QuartzProjectType);
            //手动添加
        }


        /// <summary>
        /// 关闭
        /// </summary>
        public void Stop()
        {
            if (_scheduler != null)
            {
                _scheduler.Clear();
                _scheduler.Shutdown();
            }
            _scheduler = null;
            _schedulerFactory = null;
        }
    }
}
