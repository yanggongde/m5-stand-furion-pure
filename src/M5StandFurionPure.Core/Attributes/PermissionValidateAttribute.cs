﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Core.Attributes
{
    /// <summary>
    /// 标注方法参与权限校验
    /// 前提是 开启权限验证且 未标记AllowAnonymousAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class PermissionValidateAttribute : Attribute
    {
        public PermissionValidateAttribute() { }
        public PermissionValidateAttribute(string remark)
        {
            remark = Remark;
        }

        /// <summary>
        /// 权限校验备注
        /// </summary>
        public string Remark { get; set; }
    }
}
