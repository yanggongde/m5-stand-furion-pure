﻿using FreeSql;
using Furion.DependencyInjection;
using Furion.TaskQueue;
using Furion.TimeCrontab;
using M5Extend.Tool;
using M5StandFurionPure.Core.DbEntity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Application.Service
{


    public class TestService : ITransient
    {
        private readonly BaseRepository<User> _freeSql;
        private readonly BaseRepository<IdT> _freeSql2;
        private readonly IFreeSql _sql;
        private UnitOfWorkManager _unitOfWorkManager;

        private readonly BaseRepository<Tree_node> _tree_node;
        private readonly BaseRepository<Tree_node_paths> _tree_node_paths;

        public TestService(BaseRepository<User> freeSql,
            BaseRepository<Tree_node> tree_node,
            BaseRepository<Tree_node_paths> tree_node_paths,
            BaseRepository<IdT> freeSql2,
            IFreeSql sql,
            UnitOfWorkManager unitOfWorkManager)
        {
            _freeSql = freeSql;
            _sql = sql;
            _unitOfWorkManager = unitOfWorkManager;
            _tree_node = tree_node;
            _tree_node_paths = tree_node_paths;
            _freeSql2 = freeSql2;
        }

        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="parentId">父级iD</param>
        /// <param name="name">名称</param>
        /// <returns></returns>
        public async Task Create20()
        {
            var ids = new List<IdT>();
            for (int i = 0; i < 1000; i++)
            {
                ids.Add(new IdT { Id = M5_Snowflake.CreateId() });
            }

            await _freeSql2.InsertAsync(ids);
            Console.WriteLine("##################################################ID已写入1000条##############################################");
        }

        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="parentId">父级iD</param>
        /// <param name="name">名称</param>
        /// <returns></returns>
        public async Task<long> CreateTree(long parentId, string name)
        {
            if (parentId != 0)
            {
                var parent = await _tree_node.Where(k => k.id == parentId).FirstAsync();
                if (parent == null) throw new Exception("父级不存在");
            }

            var thisId = M5_Snowflake.CreateId();
            var allParents = await _tree_node_paths.Where(k => k.descendant == parentId).ToListAsync() ?? new List<Tree_node_paths>();
            var addParentPaths = allParents.Select(k => new Tree_node_paths
            {
                ancestor = k.ancestor,
                descendant = thisId,
                distance = k.distance + 1
            }).ToList() ?? new List<Tree_node_paths>();

            await _tree_node.InsertAsync(new Tree_node
            {
                id = thisId,
                name = name,
                pid = parentId
            });
            await _tree_node_paths.InsertAsync(new Tree_node_paths
            {
                ancestor = thisId,
                descendant = thisId,
                distance = 0
            });
            if (addParentPaths.Any()) await _tree_node_paths.InsertAsync(addParentPaths);
            return thisId;
        }

        public async Task CreateUser(User user)
        {
            await _freeSql.InsertAsync(user);
        }


        public async Task UpdateUser(User user)
        {
            var userObj = await _freeSql.Select.Where(k => k.Id == user.Id).FirstAsync();
            if (userObj == null)
            {
                throw new Exception("更新用户不存在");
            }

            userObj = user.Adapt<User>();
            await _freeSql.UpdateAsync(userObj);
        }


        public async Task<List<User>> GetAllSlave(long id)
        {
            var result2 = await (from a in _freeSql
                                 where a.Id >= id
                                 select a).ToListAsync() ?? new List<User>(); ;
            //await CreateUser();
            var result = await _freeSql.Select.Where(k => k.Id >= id).WithLock(SqlServerLock.NoLock).ToListAsync() ?? new List<User>();

            //var a = int.Parse("aa");
            return result;
        }
        public async Task<List<User>> GetAllMaster(long id)
        {
            //await CreateUser();
            var result = await _freeSql.Select.Master().Where(k => k.Id >= id).WithLock(SqlServerLock.NoLock).ToListAsync() ?? new List<User>();

            //var a = int.Parse("aa");
            return result;
        }

        //[Transactional]
        public async Task UpdateUser(long id)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                try
                {
                    var user = await _sql.Select<User>().Where(k => k.Id == id).FirstAsync();
                    user.Name = "fee修改";
                    await _freeSql.UpdateAsync(user);
                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }

        }

        public async Task TaskBachInsertUser()
        {
            await TaskQueued.EnqueueAsync(async (provider, _) =>
            {
                using var scoped = provider.CreateScope();
                var repository = scoped.ServiceProvider.GetService<BaseRepository<User>>();


            }, "* * * * *");
        }
    }
}
