﻿using Furion.DependencyInjection;
using Furion.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Application.Event
{
    public class TestSourceCur
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
    public class TestEvent1 : IEventSubscriber, ISingleton
    {
        [EventSubscribe("ToDo:Create")]
        public async Task CreateToDo(EventHandlerExecutingContext context)
        {
            await Task.Delay(5000);
            Console.WriteLine($"[{DateTime.Now.ToString()}]测试ToDo:Create：{context.Source.Payload}");
            await Task.CompletedTask;
        }

        [EventSubscribe("ToDo:Create2")]
        public async Task CreateToDo2(EventHandlerExecutingContext context)
        {
            await Task.Delay(3000);
            var source = context.Source.Payload as TestSourceCur;
            Console.WriteLine($"[{DateTime.Now.ToString()}]测试ToDo:Create2：Name={source.Name},Age={source.Age}");
            await Task.CompletedTask;
        }
    }

    public class TestEvent2 : IEventSubscriber, ISingleton
    {
        private readonly IEventPublisher _eventPublisher;
        public TestEvent2(IEventPublisher eventPublisher)
        {
            _eventPublisher = eventPublisher;
        }

        [EventSubscribe("ToDo:Create3")]
        public async Task CreateToDo(EventHandlerExecutingContext context)
        {
            await Task.Delay(500);
            Console.WriteLine($"[{DateTime.Now.ToString()}]测试ToDo:Create3：{context.Source.Payload}");
            await _eventPublisher.PublishAsync(new ChannelEventSource("ToDo:Create", $"调用内部时间，2.5秒后执行"));
            await Task.CompletedTask;
        }

    }
}
