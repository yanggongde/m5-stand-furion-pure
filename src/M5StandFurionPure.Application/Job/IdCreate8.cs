﻿//using CSRedis;
//using FreeSql;
//using Furion.DependencyInjection;
//using M5Extend.Tool;
//using M5StandFurionPure.Core.Aop;
//using M5StandFurionPure.Core.DbEntity;
//using M5StandFurionPure.Core.Quartz;
//using Microsoft.Extensions.DependencyInjection;
//using Quartz;
//using SqlSugar;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace M5StandFurionPure.Application
//{

//    /// <summary>
//    /// 自动取消未支付的超时订单
//    /// DisallowConcurrentExecution标识任务执行方式为串行
//    /// </summary>
//    [QuartzJobConfig(1, 1)]
//    public class IdCreate8 : IJob, IScoped
//    {
//        private readonly BaseRepository<IdT> _freeSql;

//        public IdCreate8(BaseRepository<IdT> freeSql)
//        {
//            _freeSql = freeSql;
//        }

//        public async Task Execute(IJobExecutionContext context)
//        {
//            await Task.Delay(8901);
//            var total = 1000;
//            try
//            {
//                while (true)
//                {
//                    var ids = new List<IdT>();
//                    for (int i = 0; i < total; i++) ids.Add(new IdT() { Id = M5_Snowflake.CreateId() });
//                    await _freeSql.InsertAsync(ids);
//                    Console.WriteLine($"IdCreate8已写入1000条");
//                }
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine($"IdCreate8写入异常：{ex?.Message}");
//            }
//        }
//    }
//}
