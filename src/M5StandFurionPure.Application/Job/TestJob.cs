﻿using CSRedis;
using Furion.DependencyInjection;
using M5StandFurionPure.Core.Aop;
using M5StandFurionPure.Core.Quartz;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M5StandFurionPure.Application
{

    /// <summary>
    /// 自动取消未支付的超时订单
    /// DisallowConcurrentExecution标识任务执行方式为串行
    /// </summary>
    [DisallowConcurrentExecution]
    //[QuartzJobConfig(5, 5), UnitOfWork]
    public class TestJob : IJob, IScoped
    {

        private readonly CSRedisClient _csRedisClient;
        //private readonly ISqlSugarClient _db;
        //private readonly ISqlSugarRepository<User> _user;
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceScopeFactory _scopeFactory;

        public TestJob(CSRedisClient csRedisClient
            //,ISqlSugarClient db
            //, ISqlSugarRepository<User> user
            , IServiceProvider serviceProvider
            , IServiceScopeFactory scopeFactory
            )
        {
            _csRedisClient = csRedisClient;
            //_db = db;
            //_user = user;
            _serviceProvider = serviceProvider;
            _scopeFactory = scopeFactory;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            //using (var scope = _scopeFactory.CreateScope())
            //{
            //    var userPro = scope.ServiceProvider.GetRequiredService<ISqlSugarRepository<User>>();
            //}
            //var id = 1;
            //var user = await _user.FirstOrDefaultAsync(k => k.Id == id);
            //var userCache = await _csRedisClient.GetAsync<User>($"cache_user{id}");
            //await _user.InsertAsync(new User
            //{
            //    Id = M5_Snowflake.CreateId(),
            //    Name = "jobAdd",
            //    Age = 12
            //});

            //var a = int.Parse("dfds");

            Console.WriteLine($"\r\n当前：{DateTime.Now.ToString()}");
        }
    }
}
