﻿global using Furion;
global using Mapster;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.CodeAnalysis;
global using Microsoft.EntityFrameworkCore;
global using System.ComponentModel.DataAnnotations;

