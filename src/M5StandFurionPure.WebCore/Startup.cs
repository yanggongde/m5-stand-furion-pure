﻿using Furion;
using M5Extend.IP2Region;
using M5Extend.Tool;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace M5StandFurionPure.WebCore;

public class Startup : AppStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddCorsAccessor();
        services.AddRemoteRequest();
        services.AddResponseCompression();
        //添加ip离线查询
        services.ConfigIp2region(Path.Combine(App.HostEnvironment.ContentRootPath, "AppData/ip2region.db"));
        //需要在mongodb用户配置里面设定账号的默认库，以满足默认账号验证登录 https://www.cnblogs.com/Ricklee/p/11602020.html
        //账户创建：https://blog.51cto.com/u_16099203/6355337
        services.AddMongoDB("mongodb://admin:123456@localhost:27017/demo?authSource=admin");
        //services.AddMongoDB(new MongoClientSettings(),"demo")
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        M5_Snowflake.InitData(1, 1);
        app.UseHttpsRedirection();
        app.UseRouting();

        app.UseCorsAccessor();
        app.UseResponseCompression();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseUnifyResultStatusCodes();

        app.UseInject("swagger");

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}

