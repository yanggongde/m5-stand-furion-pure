﻿using CSRedis;
using FreeSql;
using Furion;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace M5StandFurionPure.EntityFrameWorkCore;

public class Startup : AppStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        var connectionstring = App.Configuration["ConnectionStrings:DistributedCache"] ?? throw new RedisClientException("找不到csredis配置节点");
        var client = new CSRedisClient(connectionstring);
        services.AddSingleton(factory =>
        {
            return client;
        });
        RedisHelper.Initialization(client);

        //csredis静态初始化
        //RedisHelper.Initialization(new CSRedisClient(connectionstring));

        IFreeSql fsql = new FreeSql.FreeSqlBuilder()
            .UseConnectionString(FreeSql.DataType.SqlServer, App.Configuration["ConnectionStrings:Default"])
            //.UseSlave(App.Configuration["ConnectionStrings:DefaultSlave"]) //使用从数据库，支持多个
            //.UseNameConvert(NameConvertType.PascalCaseToUnderscoreWithLower)
            //.UseAutoSyncStructure(true)
            //.UseMonitorCommand(cmd =>
            //  {
                    //这里也可以输出到性能监控
            //     // App.PrintToMiniProfiler("SQL执行", "Success", $"{cmd.CommandText}");
            //  }) //监听SQL命令对象，在执行后
           .UseNoneCommandParameter(true)
           .Build();

        //使用AOP输出性能监控
        //https://freesql.net/guide/aop.html#%E5%AE%A1%E8%AE%A1%E5%B1%9E%E6%80%A7%E5%80%BC
        fsql.Aop.CurdAfter += (s, e) =>
        {
            App.PrintToMiniProfiler("SQL执行", $"耗时：{e.ElapsedMilliseconds} ms", $"{e.Sql}");
        };
        services.AddSingleton(fsql);
        services.AddScoped<UnitOfWorkManager>();
        services.AddFreeRepository(null, typeof(Startup).Assembly);
    }


    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
    }
}
