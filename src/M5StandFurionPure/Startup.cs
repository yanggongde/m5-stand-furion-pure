﻿using Furion;
using M5Extend.Output;
using M5StandFurionPure.Core;
using M5StandFurionPure.Core.Aop.Filter;
using M5StandFurionPure.Core.Quartz;
using M5StandFurionPure.Filter;
using M5StandFurionPure.Handler;
using Microsoft.AspNetCore.Http.Features;
using Newtonsoft.Json;

namespace M5StandFurionPure;

public class Startup : AppStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        // 添加JWT授权
        services.AddJwt<JwtHandler>(enableGlobalAuthorize: true);

        services.Configure<FormOptions>(options =>
        {
            options.ValueLengthLimit = int.MaxValue;
            options.MultipartBodyLengthLimit = long.MaxValue;
        });
        services.AddControllers(options =>
        {
            options.Filters.Add(typeof(RequestValidateActionFilter));
            options.AddAopFilter();
        })
        .AddNewtonsoftJson(options =>
        {
            options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            options.SerializerSettings.Converters.Add(new M5_LongConverter());
        }).AddInjectWithUnifyResult<RESTfulResultProvider>();

        services.ConfigQuartz();
        services.AddTaskQueue();
        services.AddEventBus();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime appLifetime)
    {
        app.UseQuartz(appLifetime);
    }
}
