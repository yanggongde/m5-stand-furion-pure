﻿using CSRedis;
using Furion;
using Furion.DynamicApiController;
using M5StandFurionPure.Core.Output;
using Microsoft.AspNetCore.Mvc;

namespace M5StandFurionPure.Controllers
{
    /// <summary>
    /// API模块控制器基类
    /// </summary>
    public class BaseApiController : IDynamicApiController
    {
        protected readonly CSRedisClient _redis;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public BaseApiController()
        {
            _redis = App.RootServices.GetRequiredService<CSRedisClient>();
            _httpContextAccessor = App.RootServices.GetRequiredService<IHttpContextAccessor>();
        }

        protected WebLoginUser GetLoginUser()
        {
            return JwtLoginUser.GetWebLoginUser();
        }
    }

    /// <summary>
    /// API模块控制器基类 原始
    /// </summary>
    [Route("api/[controller]/")]
    public class BaseOriginalApiController : ControllerBase
    {
        protected readonly CSRedisClient _redis;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public BaseOriginalApiController()
        {
            _redis = App.RootServices.GetRequiredService<CSRedisClient>();
            _httpContextAccessor = App.RootServices.GetRequiredService<IHttpContextAccessor>();
        }

        protected WebLoginUser GetLoginUser()
        {
            return JwtLoginUser.GetWebLoginUser();
        }
    }
}
