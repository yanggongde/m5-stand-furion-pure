using CSRedis;
using Furion.EventBus;
using Furion.FriendlyException;
using Furion.TaskQueue;
using M5Extend.Tool;
using M5StandFurionPure.Application.Event;
using M5StandFurionPure.Application.Service;
using M5StandFurionPure.Core.Aop;
using M5StandFurionPure.Core.DbEntity;
using M5StandFurionPure.Core.DbEntity.MongoDB;
using M5StandFurionPure.Core.Output;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Driver;

namespace M5StandFurionPure.Controllers
{
    public class TestController : BaseApiController
    {
        private readonly ILogger<TestController> _logger;
        private readonly TestService _testServices;
        private readonly CSRedisClient _redis;
        private readonly IEventPublisher _eventPublisher;
        private readonly IMongoDBRepository<People, long> _repPeople;
        private readonly IMongoDBRepository<ChatMessage, long> _repChatMessage;

        public TestController(
            ILogger<TestController> logger,
            CSRedisClient redis,
            TestService testServices,
            IEventPublisher eventPublisher,
            IMongoDBRepository<People, long> repPeople,
            IMongoDBRepository<ChatMessage, long> repChatMessage)
        {
            _redis = redis;
            _logger = logger;
            _testServices = testServices;
            _eventPublisher = eventPublisher;
            _repPeople = repPeople;
            _repChatMessage = repChatMessage;
        }

        public long ChatSessionId = 5135514255544523;

        //mongo更新实例https://geek-docs.com/mongodb/mongodb-questions/882_mongodb_mongodb_c_driver_update_multiple_fields.html

        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="parentId">父级iD</param>
        /// <param name="name">名称</param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task Create20()
        {
            await _testServices.Create20();
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task SendMessage([FromBody] ChatMessage message)
        {
            message.Id = M5_Snowflake.CreateId();
            message.ChatSessionId = ChatSessionId;
            await _repChatMessage.InsertAsync(message);
        }

        /// <summary>
        /// 标记消息已读
        /// </summary>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task UpdateMessageRead()
        {
            UpdateDefinition<ChatMessage> defind = Builders<ChatMessage>.Update
                .Set(nameof(ChatMessage.IsRead), true);

            await _repChatMessage.UpdateManyAsync(k => k.ChatSessionId == ChatSessionId && k.ReceiveId == 2, defind);
        }

        [HttpGet, AllowAnonymous]
        public async Task<long> QueryNotReadMessageCount([FromQuery] long receiveId)
        {
            return await _repChatMessage.CountAsync(k => k.ChatSessionId == ChatSessionId && k.ReceiveId == receiveId && k.IsRead == false);
        }

        [HttpPost, AllowAnonymous]
        public async Task<List<ChatMessage>> QueryChatSessionMessages()
        {
            return await _repChatMessage.Entities.Find(k => k.ChatSessionId == ChatSessionId).SortBy(x => x.Id).ToListAsync();
        }

        /// <summary>
        /// 查询mongo用户
        /// </summary>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task<List<People>> GetAllPeoples()
        {
            return await _repPeople.Entities.Find(k => k.Id > 0).Skip(1).Limit(2).ToListAsync();
        }


        /// <summary>
        /// 更新单个
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task UpdatePeople(string name)
        {
            var people = await _repPeople.GetAsync(k => k.Name.Contains(name));
            if (people != null)
            {
                people.Name = $"[编辑]{people.Name}";
                await _repPeople.UpdateAsync(people);
            }
        }


        [HttpGet, AllowAnonymous]
        public async Task Remove([FromQuery] long Id)
        {
            await _repChatMessage.DeleteAsync(Id);
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task BathckUpdate()
        {
            UpdateDefinition<People> defind = Builders<People>.Update
                .Set(nameof(People.CreateTime), DateTime.Now)
                .Set(k => k.Name, "AAAAA");
            var peops = await _repPeople.UpdateManyAsync(k => k.Age < 10, defind);
        }



        /// <summary>
        /// 添加mongo用户
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        [HttpPost, AllowAnonymous]
        public async Task AddPeople(People people)
        {
            if (people != null)
            {
                people.Id = M5_Snowflake.CreateId();
                await _repPeople.InsertAsync(people);
            }
        }

        [HttpPost, AllowAnonymous]
        public async Task BatckAddPeople()
        {
            var users = new List<People>();
            for (int i = 0; i < 1000; i++)
            {
                users.Add(new People
                {
                    Id = M5_Snowflake.CreateId(),
                    Age = i,
                    CreateTime = DateTime.Now,
                    Name = $"测试用户{i}"
                });
            }

            await _repPeople.BatchInsertAsync(users);
        }

        [HttpPost, AllowAnonymous]
        public async Task<List<People>> QueryPeoples()
        {
            return await _repPeople.AsAsyncEnumerable(k => k.Age > 500);
        }

        /// <summary>
        /// 添加节点
        /// </summary>
        /// <param name="parentId">父级iD</param>
        /// <param name="name">名称</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<long> CreateTree([FromQuery] long parentId, [FromQuery] string name)
        {
            return await _testServices.CreateTree(parentId, name);
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<DateTime> TestEvent1()
        {
            await _eventPublisher.PublishAsync(new ChannelEventSource("ToDo:Create", $"事件内容{1}，5秒后执行"));
            return DateTime.Now;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<DateTime> TestEvent2()
        {
            for (int i = 0; i < 100; i++)
            {
                await _eventPublisher.PublishAsync(new ChannelEventSource("ToDo:Create2", new TestSourceCur { Age = i, Name = $"测试姓名{i}，3秒后执行" }));
            }

            return DateTime.Now;
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task TestRetryTest()
        {

            await RetryLogic.InvokeAsync(() =>
            {
                Random rd = new Random();
                var num = rd.Next(1, 10);
                var result = !num.Equals(5);
                Console.WriteLine($"抽中{num},是否重试：{result}");
                return result;
            }, () =>
            {
                throw new Exception("1-10抽中5就算 成功，重试5次依然失败，错误");
            }, 5, 2000);

            Console.WriteLine("恭喜中奖");
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<object> TestRetryException()
        {
            var lista = new List<string>();
            M5Extend.Tool.RetryException.Invoke(() =>
             {
                 lista.Add(DateTime.Now.ToString());
                 int.Parse("1g");
             }, 3, 1000, false, new Type[] { typeof(FormatException) });

            return lista;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<DateTime> GetTime()
        {
            await TaskQueued.EnqueueAsync(async (provider, _) =>
            {
                Console.WriteLine("执行一次异步方法");
            }, 100);
            return DateTime.Now;
        }

        [HttpGet]
        [AllowAnonymous, OperationLog("登录")]
        public async Task<TokenInfo> Login()
        {
            var inputs = new WebLoginUser
            {
                UserId = 1,
                UserName = "admin"
            };

            var key = $"Test_login_{inputs.UserName}";
            await RedisHelper.SetAsync(key, inputs.UserId);
            return await inputs.BuildSinaleTokenForWebLoginUser();
        }

        [HttpGet, AllowAnonymous]
        public async Task<List<User>> GetAllSlave()
        {
            return await _testServices.GetAllSlave(1);
        }

        [HttpGet, AllowAnonymous]
        public async Task<List<User>> GetAllMaster()
        {
            return await _testServices.GetAllMaster(1);
        }

        [HttpGet, UnitOfWork, OperationLog("创建用户")]
        public async Task CreateUser()
        {
            var user = GetLoginUser();
            await _testServices.CreateUser(new User
            {
                Id = M5_Snowflake.CreateId(),
                Age = 12,
                Name = "gg",
                IsDeleted = false
            });
            ///var a = int.Parse("dd");           

        }

        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost, UnitOfWork, OperationLog("更新用户")]
        public async Task UpdateUser([FromBody] User user)
        {
            await _testServices.UpdateUser(user);
        }

        [HttpGet, UnitOfWork, OperationLog("编辑用户")]
        public async Task UpdateUser(long id)
        {
            await _testServices.UpdateUser(id);
        }

        [HttpGet, AllowAnonymous, OperationLog("批量插入用户")]
        public async Task TaskBachInsertUser()
        {
            await _testServices.TaskBachInsertUser();
        }
    }
}