﻿using Furion;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using CSRedis;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Reflection;
using M5StandFurionPure.Core;
using M5StandFurionPure.Core.Output;
using M5StandFurionPure.Core.Attributes;
using M5Extend.Extensions;
using M5Standard.Core;

namespace M5StandFurionPure.Filter
{
    /// <summary>
    /// API请求后续校验
    /// 校验顺序 JWT -> 后续校验【单点校验 ->权限校验】->模型验证
    /// </summary>
    public class RequestValidateActionFilter : IAsyncActionFilter, IOrderedFilter
    {
        private readonly CSRedisClient _redis;
        /// <summary>
        /// 是否开启权限校验
        /// </summary>
        private readonly bool _isEnablePermission = (App.Configuration["Aouth:EnablePermissionCheck"] ?? throw new Exception("尚未定义EnablePermissionCheck配置")).M5_ParseBoolean();
        /// <summary>
        /// 是否开启权限校验
        /// </summary>
        private readonly bool _isEnableSingleLogin = (App.Configuration["Aouth:EnableSingleLoginCheck"] ?? throw new Exception("尚未定义EnableSingleLoginCheck配置")).M5_ParseBoolean();

        /// <summary>
        /// 
        /// </summary>
        public int Order => 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redis"></param>
        public RequestValidateActionFilter(CSRedisClient redis)
        {
            _redis = redis;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var method = actionDescriptor!.MethodInfo;
            var isAllowAnonymous = method.IsDefined(typeof(AllowAnonymousAttribute));
            //单点校验
            await CheckSingleLogin(context, isAllowAnonymous);
            //请求路由校验
            await CheckPermission(context, method, isAllowAnonymous);
            //模型验证
            ValidateModel(context);
            await next();
        }



        private void ValidateModel(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                foreach (var state in context.ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        throw new LogicException(error.ErrorMessage);
                    }
                }
            }
        }

        private async Task CheckSingleLogin(ActionExecutingContext context, bool isAllowAnonymous)
        {
            if (_isEnableSingleLogin && !isAllowAnonymous)
            {
                var singLogin = await GetLoginSingleLogin();
                var token = JwtLoginUser.GetLogindToken();
                if (singLogin == null || string.IsNullOrEmpty(token)) throw new UnauthorizedException($"Unauthorized");
                if (token != singLogin.Token)
                {
                    singLogin.Message = "当前账号已在其他设备上登录";
                    singLogin.Token = string.Empty;
                    throw new SingleLoginException(singLogin.M5_ObjectToJson());
                }
            }
        }

        private async Task CheckPermission(ActionExecutingContext context, MethodInfo method, bool isAllowAnonymous)
        {
            if (isAllowAnonymous) return;
            if (!_isEnablePermission) return;

            if (method.IsDefined(typeof(PermissionValidateAttribute)) && context.HttpContext.Request.Path.HasValue)
            {
                if (!(await GetLoginUserPermissions()).Any(k => k == context.HttpContext.Request.Path.Value.ToLower().Trim()))
                {
                    //var definds = method.GetCustomAttribute<PermissionCheckAttribute>();
                    //var remark = string.IsNullOrEmpty(definds?.Remark) ? "" : $"【{definds?.Remark}】{route}";
                    //这里如果抛出路由或备注可以更加方便处理权限问题
                    throw new NoPermissionException($"抱歉，暂无权限!  如有疑问请联系上级管理或平台客服。");
                }
            }
        }

        /// <summary>
        /// 获取当前登录用户的额API访问权限，建议从缓存中获取
        /// </summary>
        /// <returns></returns>
        private async Task<List<string>> GetLoginUserPermissions()
        {
            ////待业务实现
            return await Task.FromResult(new List<string>());
        }

        /// <summary>
        /// 获取当前登录用户的额API访问权限，建议从缓存中获取
        /// </summary>
        /// <returns></returns>
        private Task<SingleLogin> GetLoginSingleLogin()
        {
            var userId = JwtLoginUser.GetUserId();
            //写入单点登录中心
            return _redis.GetAsync<SingleLogin>(M5CacheKey.SingleLoginById.M5_Format(userId));
        }
    }
}
