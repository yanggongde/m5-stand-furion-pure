using M5StandFurionPure.Core.Logger;

var builder = WebApplication.CreateBuilder(args).Inject();
builder.Host.UseSerilogDefault(config =>
{
    config.WriteLogFileConfig(!builder.Environment.IsProduction(), "M5StandFurionPure标准开发平台", builder.Environment.EnvironmentName);
});
var app = builder.Build();
app.Run();
